"use strict";

//Переменную var, видно везде в функции
// let - не видно до обьявления и она работает только в блоке
// const - переменная, которую нельзя менять

//переменная var не имеет блочной области видимости

const name = prompt("Введите Имя!");
const age = +prompt("Введите возраст!");
if (age < 18) {
    alert("You are not allowed to visit this website")
} else if (age >= 18 && age <= 22) {
    let answer = confirm("Are you sure you want to continue?")
    if (answer) {
      alert (`Welcome, ${name}`)
    } else {
        alert("You are not allowed to visit this website")
    }
}else if (age > 22) {
    alert (`Welcome, ${name}`)
}


